#include "EpollServer.hpp"

void Usage(std::string proc)
{
    std::cout << "\n\rUsage: " << proc << "port[1024+]\n"
              << std::endl;
}

int main(int argc, char* argv[])
{
    if(argc != 2)
    {
        Usage(argv[0]);
        return 1;
    }
    auto port = std::stoi(argv[1]);
    std::unique_ptr<EpollServer> svr(new EpollServer(port));
    svr->Init();
    svr->Start();
    return 0;
}