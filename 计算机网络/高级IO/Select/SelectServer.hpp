#pragma once
#include<iostream>
#include<sys/select.h>
#include<sys/time.h>
#include "Socket.hpp"
#include "Log.hpp"

static const uint16_t defaultport = 8081;
static const int fd_max_num = (sizeof(fd_set)); //表示一个位图能存多少个比特位，也就是一个fd_set能存多少个文件描述符
int defaultfd = -1;

class SelectServer
{
public:
    SelectServer(uint16_t port = defaultport)
        : _port(port)
    {
        for(int i = 0; i < fd_max_num; i++) //初始化辅助数组
        {
            fd_array[i] = defaultfd;
            //std::cout << "fd_array[" << i << "]" << " : " << fd_array[i] << std::endl;
        }
    }
    bool Init()
    {
        _listensock.Socket();
        _listensock.Bind(_port);
        _listensock.Listen();
    }
    void Accepter()
    {
        //走到这里，就是套接字的两个事件有一个就绪了
        std::string clientip;
        uint16_t clientport = 0;
        int sock = _listensock.Accept(&clientip, &clientport); // 不会阻塞在这里，因为走到着一步就是因为底层已经有连接了，已经可以直接拿上来
        if (sock < 0) return;
        log(Info, "accept success, %s: %d, sock fd: %d", clientip.c_str(), clientport, sock);

        // 在获取到新连接后也不能直接读，因为只是把连接读上来了，但是对方可能没有发数据，所以当对方没立即发数据的时候，文件里就是空的，这时候读取会出问题哦
        int pos = 1;
        for(; pos < fd_max_num; pos++)
        {
            if (fd_array[pos] != defaultfd)
                continue; // 说明这个位置是已经被占用的位置
            else
                break;
        }
        // 走到这里有两种结果
        if (pos == fd_max_num) // 1，说明辅助数组已经被合法文件描述符占满了
        {
            log(Warning, "server is full, close %d now ", sock);
            close(sock); // 满了直接关了
        }
        else // 2，说明当前pos的位置可以被使用
        {
            fd_array[pos] = sock; // 把新获取的连接搞到数组里去
            PrintFd();
        }
    }
    void Recver(int fd, int pos)
    {
        char buffer[1024];
        ssize_t n = read(fd, buffer, sizeof(buffer) - 1); // 有bug，因为不能保证，对方通过网络发过来的报文可能不是完整的
        if (n > 0) //读成功
        {
            buffer[n] = 0;
            std::cout << "get a messge: " << buffer << std::endl;
        }
        else if (n == 0) //对方把连接关了，那么我服务器也要把套解析关了
        {
            log(Info, "client quit, me too, close fd is : %d", fd);
            close(fd);
            fd_array[pos] = defaultfd; // 这里本质是从select中移除
        }
        else //读出错，原因可能
        {
            log(Warning, "recv error: fd is : %d", fd);
            close(fd);
            fd_array[pos] = defaultfd; // 这里本质是从select中移除，因为start第一个循环会根据这个数组重新设置fd_set
        }
    }
    void Dispatcher(fd_set &rfds)
    {
        for (int i = 0; i < fd_max_num; i++)
        {
            int fd = fd_array[i];
            if (fd == defaultfd) continue; //判断文件描述符是否就绪
            //所有合法的文件描述符放在辅助数组里，select一返回，所有就绪的文件描述符就会在rfds里
            //判断我们数组里的合法的文件描述符是否也在rfds里，如果在，代表该文件描述符已经就绪：1，读事件就绪    2，连接事件就绪
            if (FD_ISSET(fd, &rfds))
            {
                //读事件就绪后也有两种情况
                if (fd == _listensock.Fd()) //1，如果等于listen套接字，就是连接事件就绪，就获取新连接，就把这个新的文件描述符继续添加进数组里
                {
                    Accepter(); // 连接管理器
                }
                else //2，如果不是linsten，那么就是别的文件描述符就绪了，也就是读事件就绪了
                {
                    Recver(fd, i);
                }
            }
        }
    }
    bool Start()
    {
        int fd = _listensock.Fd();
        //struct timeval timeout = {1, 0};

        fd_array[0] = fd;
        while(true)
        {
            fd_set rfds; //读文件描述符位图
            FD_ZERO(&rfds); //是一个宏，负责把位图清空

            //每次调用select前都对rfds进行重新设定
            int maxfd = fd_array[0];
            for(int i = 0; i < fd_max_num; i++) 
            {
                if(fd_array[i] == defaultfd) continue;
                FD_SET(fd_array[i], &rfds); //把指定的文件描述符添加到rfds中，并且此时没有设置进内核里
                if(maxfd < fd_array[i]);
                {
                    maxfd = fd_array[i];
                    log(Info, "max fd update, max fd is: %d", maxfd);
                }
            }

            //开始监听后，不能"直接"accept，因为accept就是在检测并获取listensock上面的事件，是阻塞，那么就不能一次等待多个文件描述符了，那么select就没作用了
            //是什么事件呢？就是新连接到来的事件，就是操作系统底层把三次握手完成，把新连接放到了全连接队列里，然后通过select从底层把文件拿上来
            //新连接到来，就相当于读事件就绪
            struct timeval timeout = {0, 0}; //输入输出，可能要进行周期性的重复设置
            //设为0的话，就是非阻塞了，如果不设置timeout，就是永久性阻塞，直到有事件就绪
            int n = select(maxfd + 1, &rfds, nullptr, nullptr, /*&timeout*/ nullptr);
            //如果select告诉你就绪了，那么接下来的一次读取fd的时候，不会被阻塞，就是不会再“等”了，会直接读
            switch(n)
            {
                case 0:
                    std::cout << "timeout: " << timeout.tv_sec << "." <<timeout.tv_usec << std::endl;
                    break;
                case -1:
                    std::cerr << "select error" << std::endl;
                    break;
                default:
                    //有事件就绪了,但是如果上层不处理，select就会一直通知你
                    std::cout << "get a new link " << std::endl;
                    Dispatcher(rfds); //处理事件
                    break;
            }
        }
    }
    void PrintFd()
    {
        std::cout << "online fd list: ";
        for (int i = 0; i < fd_max_num; i++)
        {
            if (fd_array[i] == defaultfd)
                continue;
            std::cout << fd_array[i] << " ";
        }
        std::cout << std::endl;
    }
    ~SelectServer()
    {
        _listensock.Close();
    }

private:
    Sock _listensock;
    uint16_t _port;
    int fd_array[fd_max_num]{defaultfd}; //辅助数组
};