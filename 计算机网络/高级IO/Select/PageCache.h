#pragma once

#include "Common.h"
#include "ObjectPool.h"
#include "PageMap.h"

// 1.page cache是一个以页为单位的span自由链表
// 2.为了保证全局只有唯一的page cache，这个类被设计成了单例模式
class PageCache
{
public:
	static PageCache* GetInstance()
	{
		return &_sInst;
	}

	// 获取从对象到span的映射
	Span* MapObjectToSpan(void* obj);

	// 释放空闲span回到Pagecache，并合并相邻的span
	void ReleaseSpanToPageCache(Span* span);

	// 获取一个k页的span
	Span* NewSpan(size_t k);

	std::mutex _pageMtx; // 全局大锁（这里与central cache不一样）
private:
	SpanList _spanLists[NPAGES];
	ObjectPool<Span> _spanPool; // 用自己写的方法申请定长span

	//std::unordered_map<PAGE_ID, Span*> _idSpanMap;  // 页号和span的映射
	//std::map<PAGE_ID, Span*> _idSpanMap;			  // 页号和span的映射
	TCMalloc_PageMap1<32 - PAGE_SHIFT> _idSpanMap;	  // 页号和span的映射（基数树代替哈希表进行优化，如果64位需要单独处理下）
	
	PageCache()
	{}
	PageCache(const PageCache&) = delete;

	static PageCache _sInst; 
};

