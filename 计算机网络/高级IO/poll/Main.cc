#include "PollServer.hpp"
#include<memory>

void Usage(std::string proc)
{
    std::cout << "\n\rUsage: " << proc << "port[1024+]\n"
              << std::endl;
}


int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(1);
    }
    //单进程演示同时接收多个连接的消息
    uint16_t port = std::stoi(argv[1]);
    std::unique_ptr<PollServer> svr(new PollServer(port));
    svr->Init();
    svr->Start(); 
    return 0;
}