#pragma once
#include<iostream>
#include<poll.h>
#include<sys/time.h>
#include "Socket.hpp"
#include "Log.hpp"

static const uint16_t defaultport = 8081;
static const int fd_max_num = 64; //表示一个evevt_fds数组能存多少个pollfd结构体
int defaultfd = -1;
int long non_event = 0;

class PollServer
{
public:
    PollServer(uint16_t port = defaultport)
        : _port(port)
    {
        for(int i = 0; i < fd_max_num; i++) //初始化辅助数组
        {
            event_fds[i].fd = defaultfd;
            event_fds[i].events = non_event;
            event_fds[i].revents = non_event;
        }
    }
    bool Init()
    {
        _listensock.Socket();
        _listensock.Bind(_port);
        _listensock.Listen();
    }
    void Accepter()
    {
        //走到这里，就是套接字的两个事件有一个就绪了
        std::string clientip;
        uint16_t clientport = 0;
        int sock = _listensock.Accept(&clientip, &clientport); // 不会阻塞在这里，因为走到着一步就是因为底层已经有连接了，已经可以直接拿上来
        if (sock < 0) return;
        log(Info, "accept success, %s: %d, sock fd: %d", clientip.c_str(), clientport, sock);

        // 在获取到新连接后也不能直接读，因为只是把连接读上来了，但是对方可能没有发数据，所以当对方没立即发数据的时候，文件里就是空的，这时候读取会出问题哦
        int pos = 1;
        for(; pos < fd_max_num; pos++)
        {
            if (event_fds[pos].fd != defaultfd)
                continue; // 说明这个位置是已经被占用的位置
            else
                break;
        }
        // 走到这里有两种结果
        if (pos == fd_max_num) // 1，说明辅助数组已经被合法文件描述符占满了
        {
            log(Warning, "server is full, close %d now ", sock);
            close(sock); 

            //select满了就直接把新链接关了，但是现在满了的时候，我们可以直接进行扩充

        }
        else // 2，说明当前pos的位置可以被使用
        {
            event_fds[pos].fd = sock;
            event_fds[pos].events = POLLIN; //关心读事件，如果还想关心读和写，就| POLLOUT
            event_fds[pos].revents = non_event;
            PrintFd();
        }
    }
    void Recver(int fd, int pos)
    {
        char buffer[1024];
        ssize_t n = read(fd, buffer, sizeof(buffer) - 1); // 有bug，因为不能保证，对方通过网络发过来的报文可能不是完整的
        if (n > 0) //读成功
        {
            buffer[n] = 0;
            std::cout << "get a messge: " << buffer << std::endl;
        }
        else if (n == 0) //对方把连接关了，那么我服务器也要把套解析关了
        {
            log(Info, "client quit, me too, close fd is : %d", fd);
            close(fd);
            event_fds[pos].fd = defaultfd; // 这里本质是从select中移除
        }
        else //读出错，原因可能
        {
            log(Warning, "recv error: fd is : %d", fd);
            close(fd);
            event_fds[pos].fd = defaultfd; // 这里本质是从select中移除，因为start第一个循环会根据这个数组重新设置fd_set
        }
    }
    void Dispatcher()
    {
        for (int i = 0; i < fd_max_num; i++)
        {
            int fd = event_fds[i].fd;
            if (fd == defaultfd) continue; //判断文件描述符是否就绪
            //判断我们数组里的合法的文件描述符是否也在rfds里，如果在，代表该文件描述符已经就绪，接下来就是检测：1，读事件就绪    2，连接事件就绪
            if (event_fds[i].revents & POLLIN) //revent按位与上POLLIN，如果为真，就是读事件就绪了
            {
                //读事件就绪后也有两种情况
                if (fd == _listensock.Fd()) //1，如果等于listen套接字，就是连接事件就绪，就获取新连接，就把这个新的文件描述符继续添加进数组里
                {
                    Accepter(); // 连接管理器
                }
                else //2，如果不是linsten，那么就是别的文件描述符就绪了，也就是读事件就绪了
                {
                    Recver(fd, i);
                }
            }
        }
    }
    bool Start()
    {
        //首先要清楚文件描述符是代表文件的，一个文件有很多属性，我们现阶段以读写属性为例
        event_fds[0].fd = _listensock.Fd();
        event_fds[0].events = POLLIN; //用户告诉内核要等待的事件
        int timeout = 3000;

        while(true)
        {
            int n = poll(event_fds, fd_max_num, timeout); //可以发现确实比select简单很多，正常，因为接口的进步就是越来越简单
            switch(n)
            {
                case 0:
                    std::cout << "time out... " << std::endl;
                    break;
                case -1:
                    std::cerr << "poll error" << std::endl;
                    break;
                default:
                    //有事件就绪了,但是如果上层不处理，select就会一直通知你
                    std::cout << "get a new link " << std::endl;
                    Dispatcher(); //处理事件
                    break;
            }
        }
    }
    void PrintFd()
    {
        std::cout << "online fd list: ";
        for (int i = 0; i < fd_max_num; i++)
        {
            if (event_fds[i].fd == defaultfd)
                continue;
            std::cout << event_fds[i].fd << " ";
        }
        std::cout << std::endl;
    }
    ~PollServer()
    {
        _listensock.Close();
    }

private:
    Sock _listensock;
    uint16_t _port;
    struct pollfd event_fds[fd_max_num]; //表示事件对应的文件描述符集合
};