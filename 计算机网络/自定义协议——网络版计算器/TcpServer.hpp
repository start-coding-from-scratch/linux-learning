#pragma once
#include "Log.hpp"
#include "Socket.hpp"
#include <signal.h>
#include <string>
#include <functional>

using func_t = std::function<std::string(std::string &package)>;

class TcpServer
{
public:
    TcpServer(uint16_t port, func_t callback)
        : _port(port), _callback(callback)
    {
    }

    bool InitServer()
    {
        // 创建，绑定，监听套接字
        _listensockfd.Socket();
        _listensockfd.Bind(_port);
        _listensockfd.Listen();
        log(Info, "Init server... done");
        return true;
    }

    void Start()
    {
        signal(SIGCHLD, SIG_IGN); // 忽略
        signal(SIGPIPE, SIG_IGN);
        while (true)
        {
            std::string clientip;
            uint16_t clientport;
            int sockfd = _listensockfd.Accept(&clientip, &clientport);

            if (sockfd < 0)
                continue;
            log(Info, "accept a new link, sockfd: %d, clientip: %s, clientport: %d", sockfd, clientip.c_str(), clientport);

            // 走到了这里就是成功获取发起连接方IP与port，后面就是开始提供服务
            if (fork() == 0)
            {
                _listensockfd.Close();
                // 进行数据运算服务
                std::string inbuffer_stream;
                while (true)
                {
                    char buffer[1280];
                    ssize_t n = read(sockfd, buffer, sizeof(buffer));
                    if (n > 0)
                    {
                        buffer[n] = 0;
                        inbuffer_stream += buffer; // 这里用+=

                        log(Debug, "debug:\n%s", inbuffer_stream.c_str());
                        while (true)
                        {
                            std::string info = _callback(inbuffer_stream);
                            // if (info.size() == 0) //ServerCal.hpp，解析报文失败的话会返回空串
                            if (info.empty()) // 空的话代表inbuffstream解析时出问题，表示不遵守协议，发不合法的报文给我，我直接丢掉不玩了
                                break;        // 不能用continue

                            log(Debug, "debug, response:\n%s", info.c_str());
                            log(Debug, "debug:\n%s", inbuffer_stream.c_str());
                            write(sockfd, info.c_str(), info.size());
                        }
                    }
                    else if (n == 0) // 读取出错
                        break;
                    else // 读取出错
                        break;
                }
                exit(0);
            }
            close(sockfd);
        }
    }

    ~TcpServer()
    {
    }

private:
    uint16_t _port;
    Sock _listensockfd;
    func_t _callback;
};