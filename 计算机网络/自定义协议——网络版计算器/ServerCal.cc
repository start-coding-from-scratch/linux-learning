#include "Log.hpp"
#include "Socket.hpp"
#include "TcpServer.hpp"
#include "Protocol.hpp"
#include "ServerCal.hpp"
#include "Deamon.hpp"

static void Usage(const std::string &proc)
{
    std::cout << "\nUsage: " << proc << "port\n\n"
              << std::endl;
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(0);
    }
    uint16_t port = std::stoi(argv[1]);
    ServerCal cal;
    TcpServer *tsvp = new TcpServer(port, std::bind(&ServerCal::Calculator, &cal, std::placeholders::_1));
    tsvp->InitServer();
    //Daemon();
    //daemon(0, 0);
    tsvp->Start();

    // Request测试--------------------
    // Request req(10, 20, '+');
    // std::string s;
    // req.Serialize(&s);
    // std::cout << "有效载荷为: " << s << std::endl;
    // s = Encode(s);
    // std::cout << "报文为：" << s;

    // std::string content;
    // bool r = Decode(s, &content); //分离报头和有效载荷
    // std::cout << "分离报头后的有效载荷为: "<< content << std::endl;
    // Request temp;

    // temp.DeSerialize(content); //解析有效载荷
    // std::cout<< "有效载荷分离后, x为: " << temp.x << " 运算符为:\"" << temp.op << "\"  y为: " << temp.y << std::endl;

    // Response测试--------------------
    // Response resp(10, 20);
    // std::string s;
    // resp.Serialize(&s);
    // std::cout << "有效载荷为: " << s << std::endl;
    // std::string package = Encode(s); //分离报头和有效载荷
    // std::cout << "报文为: " << package;
    // s = "";
    // bool r = Decode(package, &s);
    // std::cout << "分离报头后的有效载荷为: " << s << std::endl;

    // Response temp;
    // temp.DeSerialize(s); // 解析有效载荷
    // std::cout << "解析有效载荷: " << std::endl;
    // std::cout << "结果为: " << temp.result << std::endl;
    // std::cout << "错误码为: " << temp.code << std::endl;

    return 0;
}