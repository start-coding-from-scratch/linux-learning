#pragma once

#include <iostream>
#include <string>
#include <fstream>
#include <unordered_map>
#include "Log.hpp"

const std::string dictname = "./dict.txt";
const std::string sep = ":";

// yellow:黄色...
static bool Split(std::string &s, std::string *part1, std::string *part2)
{
    auto pos = s.find(sep);
    if (pos == std::string::npos)
        return false;
    *part1 = s.substr(0, pos);
    *part2 = s.substr(pos + 1);
    return true;
}

class Init
{
public:
    Init()
    {
        std::ifstream in(dictname);
        if (!in.is_open())
        {
            log(Fatal, "ifstream open %s error", dictname.c_str());
            exit(1);
        }
        std::string line;
        while (std::getline(in, line)) // 从文件流中读一行
        {
            std::string part1, part2;
            Split(line, &part1, &part2); // 以冒号为分隔符，把字典里的英文和中文隔开然后分别加载到part1和part2里
            dict.insert({part1, part2}); // 然后再把两个东东放到unordered_map和键值对里去
        }
        in.close();
    }
    std::string translation(const std::string &key)
    {
        auto iter = dict.find(key);
        if (iter == dict.end()) // 找到迭代器结尾，表示没找到
            return "Unknow";
        else
            return iter->second;
    }

private:
    std::unordered_map<std::string, std::string> dict;
};