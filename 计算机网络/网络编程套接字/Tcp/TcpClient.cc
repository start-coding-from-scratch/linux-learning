#include <iostream>
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

void Usage(const std::string &proc)
{
    std::cout << "\n\rUsage: " << proc << " serverip serverport\n"
              << std::endl;
}

int main(int argc, char *argv[])
{
    int x = 0;
    if (argc != 3)
    {
        Usage(argv[0]);
        exit(1);
    }

    std::string serverip = argv[1];
    uint16_t serverport = std::stoi(argv[2]);

    // 填写套接字信息
    struct sockaddr_in server;
    memset(&server, 0, sizeof(server));
    server.sin_family = AF_INET;
    server.sin_port = htons(serverport);
    // server.sin_addr.s_addr = inet_addr(serverip.c_str()); // 字符串转四字节，效果和inet_pton是一样的
    inet_pton(AF_INET, serverip.c_str(), &(server.sin_addr));

    while (true) // 每次进行翻译后都要重新创建套接字和建立连接，因为目前服务器只会提供一次服务
    {
        // 创捷套接字
        int sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (sockfd < 0)
        {
            std::cerr << "socket error" << std::endl;
            return 1;
        }
        int cnt = 5; // 断线重连次数
        int isreconnect = false;
        do
        {
            // tcp客户端要bind，但是不需要显示bind，客户端发起连接的时候，系统会自动进行bind，随机端口，这点和Udp是一样的
            int n = connect(sockfd, (struct sockaddr *)&server, sizeof(server));
            if (n < 0) // 连接错误
            {
                std::cerr << "connect error..., reconnect: " << cnt << std::endl;
                isreconnect = true; // 重连失败，继续重连
                cnt--;
                x = 1;
                sleep(2);
            }
            else
            {
                isreconnect = false; // 重连几次后如果成功就不再重连
                if (x == 1)
                {
                    std::cout << "reconnect success!" << std::endl;
                    x = 0;
                }
            }
            // 连接成功
        } while (cnt && isreconnect);
        if (cnt == 0) // 超过断线重连次数就直接break退出
        {
            std::cerr << "user offline... " << std::endl;
            break;
        }
        // 上面是建立确定连接过程，下面是正常提供服务
        std::string message;
        std::cout << "Please Enter# ";
        std::getline(std::cin, message);

        int n = write(sockfd, message.c_str(), message.size()); // 发消息
        if (n < 0)
        {
            std::cerr << "write error..." << std::endl;
        }

        char inbuffer[4096];
        n = read(sockfd, inbuffer, sizeof(inbuffer)); // 收消息
        if (n > 0)
        {
            inbuffer[n] = 0;
            std::cout << inbuffer << std::endl;
        }
        else
        {
            std::cerr << "read error" << std::endl;
        }

        close(sockfd);
    }
    return 0;
}