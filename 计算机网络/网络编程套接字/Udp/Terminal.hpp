#include <iostream>
#include <unistd.h>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

std::string terminal = "/dev/pts/2";

int OpenTerminal()
{
    int fd = open(terminal.c_str(), O_WRONLY);
    if (fd < 0) // 打开失败
    {
        std::cerr << "open termial error";
        return 1;
    }

    dup2(fd, 2);
    // 测试
    //  printf("hello world\n"); // 把即将打印在当前终端的内容往特定路径的终端打

    // close(fd);
    return 0;
}