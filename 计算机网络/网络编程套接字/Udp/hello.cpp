#define _WINSOCK_DEPRECATED_NO_WARNINGS 1
#include <iostream>
#include<WinSock2.h>
#include<Windows.h>
#include <cstdlib>
#include <string>
#include<cstdio>
#pragma comment(lib, "ws2_32.lib")

#include<thread>
uint16_t serverport = 8080;
std::string serverip = "58.87.91.241";

struct ThreadData
{
	struct sockaddr_in server;
	SOCKET sockfd;
	std::string serverip;
};

void* send_message(void* args)
{
	ThreadData* td = static_cast<ThreadData*>(args);
	std::string message;
	std::cout << td->serverip << " coming... " << std::endl;

	while (true)
	{
		std::cout << "Please Enter: ";
		std::getline(std::cin, message);
		sendto(td->sockfd, message.c_str(), message.size(), 0, (struct sockaddr*)&(td->server), sizeof(td->server));
	}
}
void* recv_message(void* args)
{
	ThreadData* td = static_cast<ThreadData*>(args);
	char buffer[1024];
	while (true)
	{
		memset(buffer, 0, sizeof(buffer)); //每次接收消息前清空缓冲区
		int len = sizeof(td->server);
		int s = recvfrom(td->sockfd, buffer, 1023, 0, (struct sockaddr*)&(td->server), &len);
		if (s > 0)
		{
			buffer[s] = 0;
			std::cout << buffer << std::endl;
		}
	}
}

int main()
{
	WSADATA wsd;
	WSAStartup(MAKEWORD(2, 3), &wsd);
	struct ThreadData td;

	// 构建服务器信息，因为客户端发给服务端需要知道服务端的ip和port
	memset(&td.server, 0, sizeof(td.server));
	td.server.sin_family = AF_INET;
	td.server.sin_port = htons(serverport);                  // 转成网络序列
	td.server.sin_addr.s_addr = inet_addr(serverip.c_str()); // 点分十进制的字符串转化为数字

	td.sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (td.sockfd < 0)
	{
		std::cout << "socket error" << std::endl;
		return 1;
	}
	td.serverip = serverip;
	std::thread sender(send_message, &td);
	std::thread recver(recv_message, &td);

	sender.join();
	recver.join()
		;
	WSACleanup();
	return 0;
}