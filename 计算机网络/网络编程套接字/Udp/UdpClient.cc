#include <iostream>
#include <cstdlib>
#include <unistd.h>
#include <strings.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "Terminal.hpp"

using namespace std;

void Usage(string proc)
{
    cout << "\n\rUsage: " << proc << "serverip serverport\n"
         << endl;
}

struct ThreadData
{
    struct sockaddr_in server;
    int sockfd;
    std::string serverip;
};

void *recv_message(void *args)
{
    ThreadData *td = static_cast<ThreadData *>(args);
    char buffer[1024];
    while (true)
    {
        memset(buffer, 0, sizeof(buffer)); // 每次打印时清空缓冲区
        // 数据发给服务端后，服务端也会快速把信息处理好后发回来
        struct sockaddr_in temp;
        socklen_t len = sizeof(temp);
        ssize_t s = recvfrom(td->sockfd, buffer, 1023, 0, (struct sockaddr *)&temp, &len);
        if (s > 0)
        {
            buffer[s] = 0;
            std::cerr << buffer << std::endl;
        }
    }
}
// 这里的收发函数中sockfd可以同时被读写的，因为Udp协议是全双工的，所以是线程安全的
void *send_message(void *args)
{
    OpenTerminal(); //后面介绍

    ThreadData *td = static_cast<ThreadData *>(args);
    std::string message;
    socklen_t len = sizeof(td->server);
    cout << td->serverip << " coming... " << endl;
    sendto(td->sockfd, message.c_str(), message.size(), 0, (struct sockaddr *)&(td->server), len);

    while (true)
    {
        // 客户端如何知道服务器ip和port?，所以需要客户端在启动的时候告诉我ip和port是什么
        std::cout << "Please Enter@ ";
        getline(std::cin, message); // 表示从输入流获取数据放到message里
        // 1，发数据  2，给谁发
        sendto(td->sockfd, message.c_str(), message.size(), 0, (struct sockaddr *)&(td->server), len); // 往套接字去发，发message，发送方式设为0，发给server
    }
}

// ./udpclient serverip serverport
int main(int argc, char *argv[])
{
    if (argc != 3)
    {
        Usage(argv[0]);
        exit(0);
    }
    string serverip = argv[1];
    uint16_t serverport = std::stoi(argv[2]);

    // 构建服务器信息，因为客户端发给服务端需要知道服务端的ip和port
    struct ThreadData td;
    bzero(&td.server, sizeof(td.server));
    td.server.sin_family = AF_INET;
    td.server.sin_port = htons(serverport);                  // 转成网络序列
    td.server.sin_addr.s_addr = inet_addr(serverip.c_str()); // 点分十进制的字符串转化为数字

    td.sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (td.sockfd < 0)
    {
        cout << "socket error" << endl;
        return 1;
    }

    // 服务端需要绑定端口，客户端需要绑定吗？一定要，只不过不需要用户显示的bind，一般由OS自由随机选择
    // 一个端口号只能被一个进程绑定，对于服务端是如此，对于客户端也是如此，
    // 而对于client的port是多少其实不重要，只要能保证主机上的唯一性就可以
    // 只要客户端首次发送数据的时候，系统就自动帮我们绑定了

    pthread_t recver, sender;                            // 两个线程，一个负责收消息，一个负责发消息
    pthread_create(&recver, nullptr, recv_message, &td); // recvr线程执行recv_message收消息函数
    pthread_create(&sender, nullptr, send_message, &td); // sender线程执行send+message发消息函数

    pthread_join(sender, nullptr);
    pthread_join(recver, nullptr);

    close(td.sockfd); // 不用了就和关闭文件描述符一样关闭套接字
    return 0;
}
