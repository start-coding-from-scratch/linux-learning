#include "HttpServer.hpp"
#include <unistd.h>
#include <signal.h>

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        exit(1);
    }
    uint16_t port = std::stoi(argv[1]);
    // std::unique<HttpServer> svr(new HttpServer);
    HttpServer *svr = new HttpServer(port);

    svr->Start();
    return 0;
}
