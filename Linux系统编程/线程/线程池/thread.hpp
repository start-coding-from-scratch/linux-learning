#pragma once
#include <iostream>
#include <string>
#include <functional>
#include <cstdio>

// typedef std::function<void* (void*)> fun_t; // 定义一个函数对象，这个函数返回值是void，参数类型是void*
// 但是要考虑兼容性问题，操作系统的接口是C的，所以用函数对象的话下面的创建线程可能无法识别函数对象
typedef void *(*fun_t)(void *); // 这是一个参数为void*，返回值为void*的一个函数指针，该指针类型命名为fun_t

class ThreadData
{
public:
    void *_args;
    std::string _name;
};

class Thread
{
public:
    Thread(int num, fun_t callback, void *args)
        : _func(callback)
    {
        char nameBuffer[64];
        snprintf(nameBuffer, sizeof nameBuffer, "Thread-%d", num); // 这个函数表示往特定数组写字符串，第一个表示要写的对象，第二个表示要写的数量或大小，第三个表示写的内容
        _name = nameBuffer;

        _tdata._args = args;
        _tdata._name = _name; // 方便后面使用线程名字
    }

    void start()
    {
        pthread_create(&_tid, nullptr, _func, (void *)&_tdata);
    }
    void join()
    {
        pthread_join(_tid, nullptr);
    }
    std::string GetName()
    {
        return _name;
    }
    ~Thread()
    {
    }

private:
    std::string _name;
    pthread_t _tid;
    ThreadData _tdata;
    fun_t _func; // 表示线程要执行的函数
};