#pragma once

#include <iostream>
#include <cstdio>
#include <cstdarg>
#include <ctime>
#include <string>

// 日志是有日志级别的
#define DEBUG 0
#define NORMAL 1
#define WARNING 2
#define ERROR 3
#define FATAL 4

const char *gLevelMap[] = {
    "DEBUG",
    "NORMAL",
    "WARNING",
    "ERROR",
    "FATAL"};

#define LOGFILE "./threadpool.log"

// 完整的日志功能至少包含：日志等级，时间，日志内容，并且支持用户自定义
void logMessage(int level, const char *format, ...)
{
#ifndef DEBUG_SHOW // 在makefile中加-DDEBUG_SHOW表示我想调试，就打印调试信息，如果我们#注释掉宏定义，再次make后就不打印调试信息了
    if (level == DEBUG)
        return;
#endif
    // 提取可变参数 -- 麻烦
    //  va_list ap; //是一个char*类型
    //  va_start(ap, format); //用一个参数来初始化它
    //  while(true)
    //  int x = va_arg(ap, int);  //用ap制作提取一个整数，没有int就返回NULL
    //  va_end(ap); //类似ap=nullptr

    char stdBuffer[1024];             // 日志的标准部分
    time_t timestamp = time(nullptr); // 获取时间戳
    // struct tm *localtime = localtime(&timestamp);
    // localtime->tm_hour;
    snprintf(stdBuffer, sizeof stdBuffer, "[%s] [%ld] ", gLevelMap[level], timestamp); // 把日志的标准部分搞到字符数组里

    char logBuffer[1024];
    va_list args;
    va_start(args, format); // 可变参数列表的起始地址
    // vprintf(format, args); //格式化打印可变参数列表到屏幕
    vsnprintf(logBuffer, sizeof logBuffer, format, args); // 将参数列表搞到logBuffer中
    va_end(args);

    // 把日志信息往文件里写入
    // FILE *fp = fopen(LOGFILE, "a");
    printf("%s%s\n", stdBuffer, logBuffer);
    // fprintf(fp, "%s%s\n", stdBuffer, logBuffer);
    // fclose(fp);
}