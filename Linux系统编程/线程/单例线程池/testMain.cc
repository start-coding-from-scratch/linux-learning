#include "threadPool.hpp"
#include "Task.hpp"
#include <iostream>
#include <sys/types.h>
#include <unistd.h>

int main()
{
    // logMessage(NORMAL, "%s %d %c %f \n", "这是一条日志信息", 1234, 'c', 3.14);
    srand((unsigned long)time(nullptr) ^ getpid());
    // 线程池启动
    // ThreadPool<Task> *tp = new ThreadPool<Task>();
    ThreadPool<Task>::GetInstance()->run();
    sleep(3);
    // 构建任务
    while (true)
    {
        // 生产任务的过程，制作任务的时候，要花时间
        int x = rand() % 100 + 1;
        usleep(7721);
        int y = rand() % 30 + 1;
        Task t(x, y, [](int x, int y) -> int
               { return x + y; });
        // std::cout << "任务制作完成" << x << " + " << y << " = ?" << std::endl;
        logMessage(DEBUG, "制作任务完成：%d+%d=?", x, y);

        // 推送任务到线程池中
        ThreadPool<Task>::GetInstance()->pushTask(t);
        sleep(1);
    }
    return 0;
}