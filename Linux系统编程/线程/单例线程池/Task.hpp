#include <iostream>
#include <string>
#include <functional>
#include "log.hpp"

typedef std::function<int(int, int)> func_t;

class Task
{
public:
    Task() {}
    Task(int x, int y, func_t func) : _x(x), _y(y), _func(func)
    {
    }
    void operator()(const std::string &name)
    {
        // 不规范
        //  std::cout << "线程 " << name << " 处理完成, 结果是: " << x_ << "+" << y_ << "=" << func_(x_, y_) << std::endl;
        logMessage(WARNING, "%s处理完成: %d+%d=%d | %s | %d",
                   name.c_str(), _x, _y, _func(_x, _y), __FILE__, __LINE__); // 预处理符，方便定位文件名和位置
    }

public:
    int _x;
    int _y;
    // int type;
    func_t _func;
};